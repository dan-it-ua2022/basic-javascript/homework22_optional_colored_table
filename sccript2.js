const divFromTable = document.querySelector('.create-table');

const itemTableCssName = 'table';
const itemTRCssName = 'itemTR';
const itemTDCssName = 'itemTD';
const countItem = 10;
let table;

function createContent() {
  table = createTable(itemTableCssName);

  // table.addEventListener('click', handlerTable);
  document.body.addEventListener('click', handlerBody);

  divFromTable.append(table);

  for (let i = 0; i < countItem; i++) {
    const tr = createTR(itemTRCssName);
    table.append(tr);
    for (let j = 0; j < countItem; j++) {
      const td = createTD(itemTDCssName);
      tr.append(td);
    }
  }
}

// function handlerTable(e) {
//   if (e.target.closest(`.${itemTDCssName}`)) {
//     console.log('td');
//   }
// }

function handlerBody(e) {
  if (e.target.closest(`.${itemTDCssName}`)) {
    e.target.classList.toggle('checked');
  } else if (
    !e.target.closest(`.${itemTRCssName}`) &&
    !e.target.closest(`.${itemTableCssName}`)
  ) {
    table.classList.toggle('inverted');
  }
}

function createTable(className) {
  const table = document.createElement('table');
  table.classList.add(className);
  return table;
}

function createTR(className) {
  const tr = document.createElement('tr');
  tr.classList.add(className);
  return tr;
}

function createTD(className) {
  const td = document.createElement('td');
  td.classList.add(className);
  return td;
}

createContent();
